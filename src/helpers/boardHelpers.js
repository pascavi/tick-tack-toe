export const NO_WINNER_YET = 0;
export const PLAYER_ONE_WON = 1;
export const PLAYER_TWO_WON = 2;
export const DRAW = 3;

const isPlayerOne = mark => mark === 1;
const isPlayerTwo = mark => mark === 2;
const isMarked = mark => mark > 0;

export const calculateGameWinner = boardState => {
    // check victory by rows
    for (let row = 0; row < 3; row++) {
        if (boardState[row].every(isPlayerOne)) return PLAYER_ONE_WON;
        if (boardState[row].every(isPlayerTwo)) return PLAYER_TWO_WON;
    }

    // check victory by cols
    for (let col = 0; col < 3; col++) {
        if (boardState.map(row => row[col]).every(isPlayerOne)) return PLAYER_ONE_WON;
        if (boardState.map(row => row[col]).every(isPlayerTwo)) return PLAYER_TWO_WON;
    }

    // check victory by diagonals
    if (boardState.map((row, index) => row[index]).every(isPlayerOne)) return PLAYER_ONE_WON;
    if (boardState.map((row, index) => row[index]).every(isPlayerTwo)) return PLAYER_TWO_WON;
    if (boardState.map((row, index) => row[2 - index]).every(isPlayerOne)) return PLAYER_ONE_WON;
    if (boardState.map((row, index) => row[2 - index]).every(isPlayerTwo)) return PLAYER_TWO_WON;

    //is it draw?
    if (
        boardState[0].every(isMarked) &&
        boardState[1].every(isMarked) &&
        boardState[2].every(isMarked)
    ) {
        return DRAW;
    }

    return NO_WINNER_YET;
};
