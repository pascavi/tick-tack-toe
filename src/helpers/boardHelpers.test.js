import { calculateGameWinner } from "./boardHelpers";

const NO_WINNER_YET = 0;
const PLAYER_ONE_WON = 1;
const PLAYER_TWO_WON = 2;
const DRAW = 3;

describe("Trivial case of empty board", () => {
    it("returns NO_WINNER_YET if the board is empty", () => {
        const emptyState = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
        expect(calculateGameWinner(emptyState)).toBe(NO_WINNER_YET);
    });
});

describe("Check victory by rows", () => {
    it("returns PLAYER_ONE_WON if the first row is for player one", () => {
        const boardState = [[1, 1, 1], [0, 0, 0], [0, 0, 0]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_ONE_WON);
    });

    it("returns PLAYER_ONE_WON if the second row is for player one", () => {
        const boardState = [[0, 0, 0], [1, 1, 1], [0, 0, 0]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_ONE_WON);
    });

    it("returns PLAYER_ONE_WON if the third row is for player one", () => {
        const boardState = [[0, 0, 0], [0, 0, 0], [1, 1, 1]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_ONE_WON);
    });

    it("returns PLAYER_TWO_WON if the first row is for player two", () => {
        const boardState = [[2, 2, 2], [0, 0, 0], [0, 0, 0]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_TWO_WON);
    });

    it("returns PLAYER_TWO_WON if the second row is for player two", () => {
        const boardState = [[0, 0, 0], [2, 2, 2], [0, 0, 0]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_TWO_WON);
    });

    it("returns PLAYER_TWO_WON if the third row is for player two", () => {
        const boardState = [[0, 0, 0], [0, 0, 0], [2, 2, 2]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_TWO_WON);
    });
});

describe("Check victory by cols", () => {
    it("returns PLAYER_ONE_WON if the first col is for player one", () => {
        const boardState = [[1, 0, 0], [1, 0, 0], [1, 0, 0]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_ONE_WON);
    });

    it("returns PLAYER_ONE_WON if the second col is for player one", () => {
        const boardState = [[0, 1, 0], [0, 1, 0], [0, 1, 0]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_ONE_WON);
    });

    it("returns PLAYER_ONE_WON if the third col is for player one", () => {
        const boardState = [[0, 0, 1], [0, 0, 1], [0, 0, 1]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_ONE_WON);
    });

    it("returns PLAYER_TWO_WON if the first col is for player two", () => {
        const boardState = [[2, 0, 0], [2, 0, 0], [2, 0, 0]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_TWO_WON);
    });

    it("returns PLAYER_TWO_WON if the second col is for player two", () => {
        const boardState = [[0, 2, 0], [0, 2, 0], [0, 2, 0]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_TWO_WON);
    });

    it("returns PLAYER_TWO_WON if the third col is for player two", () => {
        const boardState = [[0, 0, 2], [0, 0, 2], [0, 0, 2]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_TWO_WON);
    });
});

describe("Check victory by diagonals", () => {
    it("returns PLAYER_ONE_WON if the first diaginal is for player one", () => {
        const boardState = [[1, 0, 0], [0, 1, 0], [0, 0, 1]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_ONE_WON);
    });

    it("returns PLAYER_ONE_WON if the second diagonal is for player one", () => {
        const boardState = [[0, 0, 1], [0, 1, 0], [1, 0, 0]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_ONE_WON);
    });

    it("returns PLAYER_TWO_WON if the first diaginal is for player two", () => {
        const boardState = [[2, 0, 0], [0, 2, 0], [0, 0, 2]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_TWO_WON);
    });

    it("returns PLAYER_TWO_WON if the second diagonal is for player two", () => {
        const boardState = [[0, 0, 2], [0, 2, 0], [2, 0, 0]];
        expect(calculateGameWinner(boardState)).toBe(PLAYER_TWO_WON);
    });
});

describe("Draws", () => {
    it("returns DRAW if the board is full without winner", () => {
        const boardState = [
            [1, 1, 2], //
            [2, 1, 1], //
            [1, 2, 2] //
        ];
        expect(calculateGameWinner(boardState)).toBe(DRAW);
    });
});
