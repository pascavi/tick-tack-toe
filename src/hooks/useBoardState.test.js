import React from "react";
import { shallow } from "enzyme";
import { useBoardState } from "./useBoardState";

let boardState, setBoardSquare;
beforeEach(() => {
    testHook(() => {
        ({ boardState, setBoardSquare } = useBoardState());
    });
});

it("has empty initial state", () => {
    const emptyState = [
        [0, 0, 0], //
        [0, 0, 0], //
        [0, 0, 0] //
    ];
    expect(boardState).toMatchObject(emptyState);
});

it("modifies correctly an item", () => {
    const expectedState = [
        [1, 0, 0], //
        [0, 0, 0], //
        [0, 0, 0] //
    ];
    setBoardSquare(0, 0, 1);
    expect(boardState).toMatchObject(expectedState);
});

it("doesn't change an item if it's already marked", () => {
    const expectedState = [
        [1, 0, 0], //
        [0, 0, 0], //
        [0, 0, 0] //
    ];
    setBoardSquare(0, 0, 1);
    setBoardSquare(0, 0, 2);
    expect(boardState).toMatchObject(expectedState);
});

// ——————————————————————————————————
// Auxiliar functions for hook testing

const TestHook = ({ callback }) => {
    callback();
    return null;
};

const testHook = callback => {
    shallow(<TestHook callback={callback} />);
};
