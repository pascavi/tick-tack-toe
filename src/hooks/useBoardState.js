import { useState } from "react";
import { calculateGameWinner } from "../helpers/boardHelpers";

const initialState = [
    [0, 0, 0], //
    [0, 0, 0], //
    [0, 0, 0] //
];

export const useBoardState = () => {
    const [boardState, setBoardState] = useState(initialState);

    const setBoardSquare = (x, y, player) => {
        if (x === undefined || y === undefined || boardState[x][y] > 0) return;
        let _boardState = boardState.map(row => row.slice());
        _boardState[x][y] = player;
        setBoardState(_boardState);
        //console.log(boardState);
    };

    const resetBoard = () => {
        setBoardState(initialState);
    };

    const gameWinner = calculateGameWinner(boardState);
    return { boardState, gameWinner, setBoardSquare, resetBoard };
};

export default useBoardState;
