import React from "react";
import { mount } from "enzyme";
import BoardSquare from "./BoardSquare";

/*
const sum = (a, b) => a + b;
it("adds 1 plus 2 to equal 3", () => {
    expect(sum(1, 2)).toBe(3);
});
*/

it('includes "board-square--x" class when `markedByPlayer` state is 1', () => {
    const component = mount(<BoardSquare initialMark={1} />);
    const container = component.find('[data-test="board-square"]');
    expect(container).toHaveClassName("board-square--x");
    //console.log(component.html());
    //console.log(component.debug());
});

it('includes "board-square--o" class when `markedByPlayer` state is 2', () => {
    const component = mount(<BoardSquare initialMark={2} />);
    const container = component.find('[data-test="board-square"]');
    expect(container).toHaveClassName("board-square--o");
});

it("has `markedByPlayer` state 1 after click when `currentPlayer` prop is 1", () => {
    const component = mount(<BoardSquare currentPlayer={1} />);
    const button = component.find('[data-test="board-square-button"]');
    button.simulate("click");
    expect(component.state("markedByPlayer")).toBe(1);
});

it("has `markedByPlayer` state 2 after click when `currentPlayer` prop is 2", () => {
    const component = mount(<BoardSquare currentPlayer={2} />);
    const button = component.find('[data-test="board-square-button"]');
    button.simulate("click");
    expect(component.state("markedByPlayer")).toBe(2);
});

it("doesn't change `markedByPlayer` state on second click", () => {
    const component = mount(<BoardSquare currentPlayer={1} />);
    const button = component.find('[data-test="board-square-button"]');
    button.simulate("click");
    component.setProps({ currentPlayer: 2 });
    //console.log(component.debug());
    button.simulate("click");
    expect(component.state("markedByPlayer")).toBe(1);
});

it("calls onClickHandler on button click with proper arguments", () => {
    const onClick = jest.fn();
    const component = mount(<BoardSquare onClickHandler={onClick} x={0} y={0} />);
    const button = component.find('[data-test="board-square-button"]');
    button.simulate("click");
    expect(onClick).toHaveBeenCalledTimes(1);
    expect(onClick).toHaveBeenCalledWith(0, 0);
});

it("renders as expected on the snapshot", () => {
    const component = mount(<BoardSquare initialMark={1} />);
    expect(component).toMatchSnapshot();
});
