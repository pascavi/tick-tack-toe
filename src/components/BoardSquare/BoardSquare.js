import React from "react";
import classNames from "classnames";

export class BoardSquare extends React.Component {
    constructor(props) {
        super(props);
        this.state = { markedByPlayer: props.initialMark || 0 };
    }

    componentDidUpdate = () => {
        if (this.props.turn === 1 && this.state.markedByPlayer > 0)
            this.setState({ markedByPlayer: 0 });
    };

    _onClick = () => {
        if (this.state.markedByPlayer > 0 || this.props.disabled) return;
        this.setState({
            markedByPlayer: this.props.currentPlayer || 1
        });
        if (this.props.onClickHandler) this.props.onClickHandler(this.props.x, this.props.y);
    };

    render = props => {
        const className = classNames("board-square", {
            "board-square--x": this.state.markedByPlayer === 1,
            "board-square--o": this.state.markedByPlayer === 2
        });

        return (
            <div className={className} data-test="board-square">
                <button onClick={this._onClick} data-test="board-square-button" />
            </div>
        );
    };
}

export default BoardSquare;
