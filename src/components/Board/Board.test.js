import React from "react";
import { shallow, mount } from "enzyme";
import Board, { getCurrentPlayer } from "./Board";
import BoardSquare from "../BoardSquare";
import * as CustomHook from "../../hooks/useBoardState";

describe("The board has 9 squares", function() {
    it("renders 9 boxes", () => {
        const component = shallow(<Board />);
        const boxes = component.find('[data-test="board-square-wrapper"]');
        expect(boxes.length).toBe(9);
    });
});

describe("Players' turn alternate over the game turns", function() {
    it("sets Player 1's turn on turn number 1", () => {
        const player = getCurrentPlayer(1);
        expect(player).toBe(1);
    });

    it("sets Player 2's turn on turn number 2", () => {
        const player = getCurrentPlayer(2);
        expect(player).toBe(2);
    });

    it("sets Player 1's turn on turn number 27", () => {
        const player = getCurrentPlayer(3);
        expect(player).toBe(1);
    });
});

describe("Game state is updated after a player move", function() {
    it("increases turn after a player move", () => {
        // setup an spy mocking setTurn
        const setTurn = jest.fn();
        const useTurnSpy = jest.spyOn(React, "useState");
        useTurnSpy.mockImplementation(init => [init, setTurn]);

        // setup an spy mocking setBoardSquare
        const setBoardSquare = jest.fn();
        const useBoardStateSpy = jest.spyOn(CustomHook, "useBoardState");
        useBoardStateSpy.mockImplementation(() => ({
            setBoardSquare,
            gameWinner: 0,
            resetBoard: () => {}
        }));

        // mount the component and get a reference to the first square
        const component = mount(<Board />);
        const square = component.find(BoardSquare).first();

        // simulate a call to the on click handler
        square.props().onClickHandler(0, 0);

        // expect turn to have been increased
        expect(setTurn).toHaveBeenCalledTimes(1);
        expect(setTurn).toHaveBeenCalledWith(2);

        // expect setBoardSquare to have been called with the first square coords
        expect(setBoardSquare).toHaveBeenCalledTimes(1);
        expect(setBoardSquare).toHaveBeenCalledWith(0, 0, 1);

        // restore the mocked parts
        useTurnSpy.mockRestore();
        useBoardStateSpy.mockRestore();
    });

    it("Game turn panel is shown if gams is not over", () => {
        const component = mount(<Board />);
        const panel = component.find('[data-test="turn-info-panel"]');
        expect(panel.length).toBe(1);
    });
});

describe("Game is reset correctly", function() {
    const setTurn = jest.fn();
    const resetBoard = jest.fn();
    let useTurnSpy, useBoardStateSpy, component;

    beforeEach(() => {
        useTurnSpy = jest
            .spyOn(React, "useState")
            .mockImplementation(init => [init, setTurn]);
        useBoardStateSpy = jest
            .spyOn(CustomHook, "useBoardState")
            .mockImplementation(() => ({
                setBoardSquare: () => {},
                gameWinner: 1,
                resetBoard
            }));
        component = mount(<Board />);
    });

    afterEach(() => {
        jest.clearAllMocks();
        useTurnSpy.mockRestore();
        useBoardStateSpy.mockRestore();
    });

    it("Game turn panel is shown if we have a winner", () => {
        const panel = component.find('[data-test="game-over-panel"]');
        expect(panel.length).toBe(1);
    });

    it("resets game after `play again` is clicked", () => {
        const button = component.find('[data-test="game-over-panel"] button');
        button.props().onClick();
        expect(setTurn).toHaveBeenCalledTimes(1);
        expect(setTurn).toHaveBeenCalledWith(1);
        expect(resetBoard).toHaveBeenCalledTimes(1);
    });
});
