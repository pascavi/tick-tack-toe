import React from "react";
import BoardSquare from "../BoardSquare";
import { useBoardState } from "../../hooks/useBoardState";
import { NO_WINNER_YET } from "../../helpers/boardHelpers";

export const getCurrentPlayer = turn => {
    return ((turn - 1) % 2) + 1;
};

const Board = props => {
    const [turn, setTurn] = React.useState(props.initialTurn || 1);
    const { setBoardSquare, gameWinner, resetBoard } = useBoardState();

    // on board square click
    const _onBoardSquareClick = (x, y) => {
        //update the turn
        setTurn(turn + 1);
        //update the board state
        setBoardSquare(x, y, getCurrentPlayer(turn));
    };

    // on play again button click
    const _onPlayAgainClick = () => {
        //reset turn
        setTurn(1);
        //reset board
        resetBoard();
    };

    //Create boardSquare components array
    const currentPlayer = getCurrentPlayer(turn);
    const boardSquares = [];
    for (let i = 0; i < 3; i++)
        for (let j = 0; j < 3; j++) {
            boardSquares.push(
                <div data-test="board-square-wrapper" key={3 * i + j}>
                    <BoardSquare
                        x={i}
                        y={j}
                        currentPlayer={currentPlayer}
                        onClickHandler={_onBoardSquareClick}
                        turn={turn}
                        disabled={gameWinner > NO_WINNER_YET}
                    />
                </div>
            );
        }
    //render
    return (
        <>
            {/* the board */}
            <div className="board-wrapper">
                <div className="board">{boardSquares}</div>
            </div>
            {/* the turn panel */}
            {gameWinner === NO_WINNER_YET && (
                <div className="turn-info-panel" data-test="turn-info-panel">
                    <p>Turn {turn} </p>
                    <p>Player {currentPlayer}</p>
                </div>
            )}
            {/* the end game panel */}
            {gameWinner > NO_WINNER_YET && (
                <div className="game-over-panel" data-test="game-over-panel">
                    <p>Game Over</p>
                    <button onClick={_onPlayAgainClick}>Play Again!</button>
                </div>
            )}
        </>
    );
};

export default Board;
